# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-botocore
# Verify required version from py3-boto3 on this package before upgrading
pkgver=1.34.121
pkgrel=0
pkgdesc="The low-level, core functionality of Boto3"
url="https://github.com/boto/botocore"
arch="noarch"
license="Apache-2.0"
depends="
	py3-certifi
	py3-dateutil
	py3-jmespath
	py3-urllib3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/b/botocore/botocore-$pkgver.tar.gz"
builddir="$srcdir/botocore-$pkgver"
options="!check" # take way too long

replaces=py-botocore # Backwards compatibility
provides=py-botocore=$pkgver-r$pkgrel # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
e28ecbb0dac818f122732e8dd5f5c24f4e482fedadf721de4739c14cdb0ee5992aace4418c8ac98cb910d6f732e5d781e360294432de0b5e14e3eb1f3356488d  botocore-1.34.121.tar.gz
"
